# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from enum import Enum
from typing import Iterable, Sequence, Union, Tuple, NamedTuple, Optional, Any, TypeVar  # works on 3.5.2
from abc import abstractmethod


class BasePlotError(Exception):
    pass


class PlotParameterError(BasePlotError, ValueError):
    pass


class PlotDataShapeError(BasePlotError, IndexError):
    pass


Numeric = Union[int, float, np.int, np.float]


class Plot:
    Point = TypeVar('Point')
    Kind = TypeVar('Kind')
    Axes = TypeVar('Axes')
    Source = Union[Iterable[Point], Sequence[Iterable[Numeric]]]
    _DataPart = NamedTuple('__DataPart', [('data', Source),
                                          ('detached', bool),
                                          ('kind', Kind),
                                          ('parts', Optional[Sequence[int]]),
                                          ('args', Any),
                                          ('kwargs', Any)])

    def __init__(self, title: str = None, *, caption: str = None, xl: str = None, yl: str = None):
        self._datas = {}
        self._bools = {}
        self.__title = str(title)
        self.__caption = str(caption)
        self.__xl = str(xl)
        self.__yl = str(yl)
        self.__counter = 0

    @abstractmethod
    def _data_part_create(self, data: Source, kind: Kind, *args, **kwargs) -> _DataPart:
        ...

    @abstractmethod
    def _data_part_draw(self, ax: Axes, dp: _DataPart):
        ...

    @abstractmethod
    def _before_drawing(self) -> Axes:
        ...

    @abstractmethod
    def _after_drawing(self, ax: Axes):
        ...

    def data_add(self, data: Source, kind: Kind = None, *args, **kwargs) -> int:
        index = self.__counter
        self.__counter += 1
        self._datas[index] = self._data_part_create(data, kind, *args, **kwargs)
        self._bools[index] = True
        return index

    def data_del(self, dataind: int):
        if dataind in self._datas:
            del self._datas[dataind]
            del self._bools[dataind]

    def data_set_draw(self, dataind: int, draw: bool = True):
        if dataind in self._datas:
            self._bools[dataind] = draw

    @property
    def title(self) -> str:
        return self.__title

    @title.setter
    def title(self, value):
        self.__title = str(value)

    @property
    def caption(self) -> str:
        return self.__caption

    @caption.setter
    def caption(self, value):
        self.__caption = str(value)

    @property
    def xlabel(self) -> str:
        return self.__xl

    @xlabel.setter
    def xlabel(self, value):
        self.__xl = str(value)

    @property
    def ylabel(self) -> str:
        return self.__yl

    @ylabel.setter
    def ylabel(self, value):
        self.__yl = str(value)

    def draw(self):
        ax = self._before_drawing()

        for dataind in self._datas:
            if self._bools[dataind]:
                self._data_part_draw(ax, self._datas[dataind])

        self._after_drawing(ax)

        if self.__xl is not None:
            ax.set_xlabel(self.__xl)

        if self.__yl is not None:
            ax.set_ylabel(self.__yl)

        if self.__caption is not None:
            plt.gcf().canvas.set_window_title(self.__caption)

        if self.__title is not None:
            plt.title(self.__title)

        plt.show()


Point3D = Tuple[Numeric, Numeric, Numeric]


class Kind3D(Enum):
    Wireframe = 'wireframe',
    Scatter = 'scatter'
    Line = 'line'


class Plot3D(Plot):
    Point = Point3D
    Kind = Kind3D
    Axes = Axes3D

    def __init__(self, title: str = None, *, caption: str = None, xl: str = None, yl: str = None, zl: str = None):
        super().__init__(title, caption=caption, xl=xl, yl=yl)
        self.__zl = str(zl)

    def _data_part_create(self, data: Plot.Source, kind: Kind3D = Kind3D.Line, parts: Sequence[int] = None, *args,
                          **kwargs) -> Plot._DataPart:
        if kind is None:
            kind = Kind3D.Line
        elif kind not in Kind3D:
            raise PlotParameterError('Parameter `kind` has illegal value `%s`.' % kind)

        if kind == Kind3D.Wireframe and parts is None:
            raise PlotParameterError('Parameter `parts` should be specified when `kind` is `Wireframe`.')

        if kind != Kind3D.Wireframe and parts is not None:
            parts = None

        if isinstance(data, Sequence) and len(data) == 3:
            detached = True
            data = (data[0], data[1], data[2])
        elif isinstance(data, Iterable):
            detached = False
        else:
            raise PlotParameterError(
                'Parameter `data` has illegal structure (should be either `Iterable[Point]` or `Sequence[Iterable[Numeric]]`).')

        return self._DataPart(data=data, detached=detached, kind=kind, parts=parts, args=args, kwargs=kwargs)

    def _data_part_draw(self, ax: Axes, dp: Plot._DataPart):
        if dp.kind == Kind3D.Wireframe:
            shape = (len(dp.parts), max(dp.parts))
            nX = np.empty(shape, np.float32)
            nY = np.empty(shape, np.float32)
            nZ = np.empty(shape, np.float32)
            i = zip(dp.data[0], dp.data[1], dp.data[2]) if dp.detached else iter(dp.data)

            for xj in range(shape[0]):
                for yj in range(dp.parts[xj]):
                    try:
                        nX[xj, yj], nY[xj, yj], nZ[xj, yj] = next(i)
                    except StopIteration as si_error:
                        raise PlotDataShapeError(
                            'Not enough values in given arrays according to `parts` parameter') from si_error

            ax.plot_wireframe(nX, nY, nZ, *dp.args, **dp.kwargs)
        else:
            if dp.detached:
                xs, ys, zs = dp.data[0], dp.data[1], dp.data[2]
            else:
                xs, ys, zs = [], [], []

                for val in dp.data:
                    xs.append(val[0])
                    ys.append(val[1])
                    zs.append(val[2])

            if dp.kind == Kind3D.Scatter:
                ax.scatter(xs, ys, zs, *dp.args, **dp.kwargs)
            elif dp.kind == Kind3D.Line:
                ax.plot(xs, ys, zs=zs, *dp.args, **dp.kwargs)

    def _before_drawing(self) -> Axes:
        return self.Axes(plt.figure())

    def _after_drawing(self, ax: Axes):
        if self.__zl is not None:
            ax.set_zlabel(self.__zl)

    @property
    def zlabel(self) -> str:
        return self.__zl

    @zlabel.setter
    def zlabel(self, value):
        self.__zl = str(value)


Point2D = Tuple[Numeric, Numeric]


class Kind2D(Enum):
    Scatter = 'scatter'
    Line = 'line'


class Plot2D(Plot):
    Point = Point2D
    Kind = Kind2D
    Axes = plt.Axes

    def __init__(self, title: str = None, *, caption: str = None, xl: str = None, yl: str = None, grid: bool = True):
        super().__init__(title, caption=caption, xl=xl, yl=yl)
        self.__grid = grid

    def _data_part_create(self, data: Plot.Source, kind: Kind2D = Kind2D.Line, parts: Sequence[int] = None, *args,
                          **kwargs) -> Plot._DataPart:
        if kind is None:
            kind = Kind2D.Line
        elif kind not in Kind2D:
            raise PlotParameterError('Parameter `kind` has illegal value `%s`.' % kind)

        if isinstance(data, Sequence) and len(data) == 2:
            detached = True
            data = (data[0], data[1])
        elif isinstance(data, Iterable):
            detached = False
        else:
            raise PlotParameterError(
                'Parameter `data` has illegal structure (should be either `Iterable[Point]` or `Sequence[Iterable[Numeric]]`).')

        return self._DataPart(data=data, detached=detached, kind=kind, parts=None, args=args, kwargs=kwargs)

    def _data_part_draw(self, ax: Axes, dp: Plot._DataPart):
        if dp.detached:
            xs, ys = dp.data[0], dp.data[1]
        else:
            xs, ys = [], []

            for val in dp.data:
                xs.append(val[0])
                ys.append(val[1])

        if dp.kind == Kind2D.Scatter:
            ax.scatter(xs, ys, *dp.args, **dp.kwargs)
        elif dp.kind == Kind2D.Line:
            ax.plot(xs, ys, *dp.args, **dp.kwargs)

    def _before_drawing(self) -> Axes:
        _, ax = plt.subplots()
        return ax

    def _after_drawing(self, ax: Axes):
        plt.grid(self.__grid)

    def data_del(self, dataind: int):
        if dataind in self._datas:
            del self._datas[dataind]
            del self._bools[dataind]

    def data_set_draw(self, dataind: int, draw: bool = True):
        if dataind in self._datas:
            self._bools[dataind] = draw

    @property
    def grid(self):
        return self.__grid

    @grid.setter
    def grid(self, value):
        self.__grid = bool(value)
